/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yrmg.arapp;

import com.yrmg.impl.DocenteDaoImpl;
import com.yrmg.pojo.Docente;
import com.yrmg.pojo.Facultad;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Yasser
 */
public class Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Docente docente = new Docente(1332, "Ana", "Conda", "001-110295-0000D");

        DocenteDaoImpl ddao = new DocenteDaoImpl();
        List<Docente> docentes;
        try {
            docente.setAddress("Villa Valezka 2");
            docente.setBirthday(new SimpleDateFormat("dd/MM/yyyy").parse("11/02/1995").getTime());
            docente.setContractDate(new SimpleDateFormat("dd/MM/yyyy").parse("01/02/2005").getTime());
            docente.setContractType("Tiempo completo");
            docente.setCountry("Nicaragua");
            docente.setEmail("ana.conda@fcys.uni.edu.ni");
            docente.setFacultad(new Facultad("FCyS","Ciencias y Sistemas"));
            docente.setInss("1677195");
            docente.setPosition("Tiempo Completo");
            docente.setTitle("Ing.");
            docente.setUbication("Matematica");
            
            //ddao.save(docente);
            
            Docente doc = ddao.get(1).orElse(null);
            if(doc == null){
                return;
            }
            System.out.println(doc.toString());
//            docentes = ddao.getAll();
//            for (Docente d : docentes) {
//                System.out.println(d.toString());
//            }
        } catch (IOException ex) {
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
