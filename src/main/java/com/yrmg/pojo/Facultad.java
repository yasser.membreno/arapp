/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yrmg.pojo;

/**
 *
 * @author Yasser
 */
public class Facultad {
    private int id;
    private String code;
    private String name;

    public Facultad(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Facultad{" + "id=" + id + ", code=" + code + ", name=" + name + '}';
    }
    
    
    
}
