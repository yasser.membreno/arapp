/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yrmg.pojo;

/**
 *
 * @author Yasser
 */
public class Docente {
    private int id; // 10 char
    private int code; // 10 char
    private String names; // 30 char
    private String lastnames; // 30
    private long birthday;// 15
    private String country;// 15
    private String town;// 15
    private String address;// 200
    private String dni;// 20
    private String inss;// 10
    private String email;// 30
    private long contractDate;// 15
    private String contractType;// 30    
    private String ubication;// 30
    private String position;// 30
    private String title;// 30
    private Facultad facultad;// 30

    public Docente(int code, String names, String lastnames, String dni) {
        this.code = code;
        this.names = names;
        this.lastnames = lastnames;
        this.dni = dni;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getLastnames() {
        return lastnames;
    }

    public void setLastnames(String lastnames) {
        this.lastnames = lastnames;
    }

    public long getBirthday() {
        return birthday;
    }

    public void setBirthday(long birthday) {
        this.birthday = birthday;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getInss() {
        return inss;
    }

    public void setInss(String inss) {
        this.inss = inss;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getContractDate() {
        return contractDate;
    }

    public void setContractDate(long contractDate) {
        this.contractDate = contractDate;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getUbication() {
        return ubication;
    }

    public void setUbication(String ubication) {
        this.ubication = ubication;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Facultad getFacultad() {
        return facultad;
    }

    public void setFacultad(Facultad facultad) {
        this.facultad = facultad;
    }

    @Override
    public String toString() {
        return "Docente{" + "id=" + id + ", code=" + code + ", names=" + names + ", lastnames=" + lastnames + ", birthday=" + birthday + ", country=" + country + ", town=" + town + ", address=" + address + ", dni=" + dni + ", inss=" + inss + ", email=" + email + ", contractDate=" + contractDate + ", contractType=" + contractType + ", ubication=" + ubication + ", position=" + position + ", title=" + title + '}';
    }
    
    
    
}
