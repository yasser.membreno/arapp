/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yrmg.impl;

import com.google.gson.Gson;
import com.yrmg.dao.DocenteDao;
import com.yrmg.pojo.Docente;
import com.yrmg.util.IOCollections;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 *
 * @author Yasser
 */
public class DocenteDaoImpl implements DocenteDao {

    private File fhead;
    private File fdata;
    private RandomAccessFile rafhead;
    private RandomAccessFile rafdata;
    private final int SIZE = 900;
    private Gson gson;
    private IOIndex ioIndex;

    public DocenteDaoImpl() {
        gson = new Gson();
        ioIndex = new IOIndex();
    }

    private void open() throws FileNotFoundException, IOException {
        fhead = new File("docente.head");
        fdata = new File("docente.data");

        rafhead = new RandomAccessFile(fhead, "rw");
        rafdata = new RandomAccessFile(fdata, "rw");

        if (rafhead.length() <= 0) {
            rafhead.seek(0);
            rafhead.writeInt(0);
            rafhead.writeInt(0);
        }
    }

    private void close() throws IOException {
        if (rafhead != null) {
            rafhead.close();
        }
        if (rafdata != null) {
            rafdata.close();
        }
    }
    
    private Field getField(String fieldName) throws NoSuchFieldException{        
        Field field = Docente.class.getField(fieldName);
        return field;        
    }
    
    public void createIndex(String name) throws NoSuchFieldException, IOException{
        Field fieldName = getField(name);
        ioIndex.createIndex(fieldName);
    }

    @Override
    public Docente find(Comparator<Docente> fun) throws IOException {
        Docente d = null;
        
        return d;
    }

    @Override
    public List<Docente> any(Predicate<Docente> fun) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Optional<Docente> get(long id) throws IOException {
        open();        
        rafhead.seek(0);
        int n = rafhead.readInt();
        int k = rafhead.readInt();

        if(id > k || id <= 0){
            return Optional.empty();
        }        
 
        int index = IOCollections.binarySearch(rafhead,(int) id, 0, n-1);
        long posHead = 8 + index * 4;
        rafhead.seek(posHead);
        int code = rafhead.readInt();
        
        if (code != id) {
            return Optional.empty();
        }

        long posData = (id - 1) * SIZE;
        rafdata.seek(posData);

        String jsonDocente = rafdata.readUTF();        
        Docente d = gson.fromJson(jsonDocente, Docente.class);
        
        close();
        return Optional.of(d);
    }

    @Override
    public List<Docente> getAll() throws IOException {
        open();
        List<Docente> docentes = new ArrayList<>();
        rafhead.seek(0);
        int n = rafhead.readInt();
        int k = rafhead.readInt();

        for (int i = 0; i < n; i++) {
            long poshead = 8 + 4 * i;
            rafhead.seek(poshead);

            int index = rafhead.readInt();
            long posdata = (index - 1) * SIZE;
            rafdata.seek(posdata);

            String jsonDocente = rafdata.readUTF();            
            Docente d = gson.fromJson(jsonDocente, Docente.class);
            docentes.add(d);
        }

        close();
        return docentes;
    }

    @Override
    public void save(Docente t) throws IOException {
        open();
        rafhead.seek(0);
        int n = rafhead.readInt();
        int k = rafhead.readInt();

        long posdata = k * SIZE;

        t.setId(k + 1);
        String jsonDocente = gson.toJson(t);

        rafdata.seek(posdata);
        rafdata.writeUTF(jsonDocente);

        long poshead = 8 + 4 * n;

        rafhead.seek(0);
        rafhead.writeInt(++n);
        rafhead.writeInt(++k);

        rafhead.seek(poshead);
        rafhead.writeInt(k);
        close();
    }

    @Override
    public boolean delete(Docente t) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(Docente t) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
