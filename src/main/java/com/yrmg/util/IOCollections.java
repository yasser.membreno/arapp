/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yrmg.util;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Comparator;

/**
 *
 * @author Yasser
 */
public final class IOCollections {

    public static int binarySearch(RandomAccessFile raf, int key, int low, int high) throws IOException {
        int index = -1;

        while (low <= high) {
            int mid = (low + high) / 2;
            long pos = 8 + 4 * mid;
            raf.seek(pos);
            int id = raf.readInt();
            if (id < key) {
                low = mid + 1;
            } else if (id > key) {
                high = mid - 1;
            } else if (id == key) {
                index = mid;
                break;
            }
        }
        return index;
    }
    
    public static int binarySearch(RandomAccessFile raf, String key, int low, int high) throws IOException {
        int index = -high;

        while (low <= high) {
            int mid = (low + high) / 2;
            long pos = 8 + 4 * mid;
            raf.seek(pos);
            String id = raf.readUTF();
            if (id.compareTo(key) < 0) {
                low = mid + 1;
            } else if (id.compareTo(key) > 0) {
                index = -mid;
                high = mid - 1;                
            } else if (id.compareTo(key) == 0) {
                index = mid;
                break;
            }
        }
        return index;
    }

    public static <T> int binarySearch(RandomAccessFile raf,T key, Comparator<T> c, int low, int high) throws IOException {
        int index = -high;

        while (low <= high) {
            int mid = (low + high) / 2;
            long pos = 8 + 4 * mid;
            raf.seek(pos);
            T id = (T) raf.readUTF();
            if (c.compare(id,key) < 0) {
                low = mid + 1;
            } else if (c.compare(id,key) > 0) {
                index = -mid;
                high = mid - 1;                
            } else if (c.compare(id,key) == 0) {
                index = mid;
                break;
            }
        }
        return index;
    }
   
}
