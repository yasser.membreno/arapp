/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yrmg.dao;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Yasser
 */
public interface Dao<T> {
    Optional<T> get(long id) throws IOException;
    List<T> getAll() throws IOException;
    void save(T t) throws IOException;
    boolean delete(T t) throws IOException;
    boolean update(T t) throws IOException;    
}
