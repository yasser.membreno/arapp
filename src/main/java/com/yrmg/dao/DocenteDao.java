/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yrmg.dao;

import com.yrmg.pojo.Docente;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

/**
 *
 * @author Yasser
 */
public interface DocenteDao extends Dao<Docente>{
    Docente find(Comparator<Docente> fun) throws IOException;
    List<Docente> any(Predicate<Docente> fun) throws IOException;
}
